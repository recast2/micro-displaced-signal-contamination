FROM atlasamglab/root-base:root6.26.10-python3.10
ADD . /get-sig-cont
WORKDIR /get-sig-cont

USER root

SHELL ["/bin/bash", "-c"]

#add nobody to the root group

RUN usermod -aG root nobody && \
    chsh -s /bin/bash nobody && \ 
    su nobody && \ 
    id -Gn

RUN chown -R nobody /get-sig-cont && \
    make setup







