This package takes a text file with yields in each set of regions and returns a text file with these numbers printed nicely and signal contamination varaibles.


If you want to run using a docker image:

```
docker pull atlasamglab/root-base:root6.26.10-python3.10
docker run --rm -it -v $PWD:/Test atlasamglab/root-base:root6.26.10-python3.10 bash
cd Test
make setup
python get_signal_contamination.py yields.txt
```

Otherwise:
```
source env_setup.sh
make setup
```
to run:
```
python get_signal_contamination.py yields.txt
```

yields is just an example, add in your yields file.

The signal contamination variables are defined as:

$\delta_i = \frac{N_\text{sig}^i}{N_\text{bkg}^i}$ for $i \in \{A, B, C, E\}$

where $N^i$ is the number of events in region i in signal or data, as denoted. $N_\text{bkg}^i$ is $N_\text{data}^i-N_\text{sig}^i$.

r is defined as:

$r= \delta^{-1}_H(\delta_B + \delta_C + \delta_E - 2 \cdot \delta_A)$

See [here](http://dx.doi.org/10.1103/PhysRevD.103.035021) or int note [here](https://cds.cern.ch/record/2765701/?) for more info.

If $N_\text{sig}^i\geq N_\text{data}^i$ then $r=-99$, indicating that your signal contamination is ridiculous. 

