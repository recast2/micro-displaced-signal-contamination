from setuptools import setup

setup(
    name="micro-displaced-signal-contamination",
    version="0.1",
    author="Melissa Yexley",
    author_email="m.yexley@cern.ch",
    #packages=[],
    install_requires=["uncertainties", "black"],
)
