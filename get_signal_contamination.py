import uncertainties
from uncertainties import ufloat
from math import sqrt
import os, sys

def get_delta_i(Ni_data,Ni_sig):

    diff = Ni_data-Ni_sig
    if diff <= 0:
      return ufloat(-99,99)
    delta_i = Ni_sig / diff
    return delta_i

yields = sys.argv[1]

def getExpandedScientificNotation(flt):
    was_neg = False
    if not ("e" in flt):
        return flt
    if flt.startswith('-'):
        flt = flt[1:]
        was_neg = True 
    str_vals = str(flt).split('e')
    coef = float(str_vals[0])
    exp = int(str_vals[1])
    return_val = ''
    if int(exp) > 0:
        return_val += str(coef).replace('.', '')
        return_val += ''.join(['0' for _ in range(0, abs(exp - len(str(coef).split('.')[1])))])
    elif int(exp) < 0:
        return_val += '0.'
        return_val += ''.join(['0' for _ in range(0, abs(exp) - 1)])
        return_val += str(coef).replace('.', '')
    if was_neg:
        return_val='-'+return_val
    return return_val

def print_2siffigs(N):
    sig_figs_val='{:.2}'.format(float(N.n))
    sig_figs_uncert='{:.2}'.format(float(N.s)) 
    val_no_e=getExpandedScientificNotation(str(sig_figs_val))
    uncert_no_e=getExpandedScientificNotation(str(sig_figs_uncert))
    return val_no_e + ' +/- ' + uncert_no_e

def print_2dp(N):
    dp_val='{:.2f}'.format(float(N.n))
    dp_uncert='{:.2f}'.format(float(N.s)) 
    val_no_e=getExpandedScientificNotation(str(dp_val))
    uncert_no_e=getExpandedScientificNotation(str(dp_uncert))
    return val_no_e + ' +/- ' + uncert_no_e

with open('output.txt', 'w') as f:
    with open(yields) as file:
        lines = file.read().splitlines()
        numsR1 = lines[1]
        numsR2 = lines[2]
        numsR3 = lines[3]
        dict_of_nums = {"R1" : numsR1, "R2" : numsR2, "R3" : numsR3}

        for nums in dict_of_nums:
            nums_split = dict_of_nums[nums].split(',')

            H_est_bkg = ufloat(float(nums_split[0]),float(nums_split[1]))
        
            A_data = ufloat(float(nums_split[2]),sqrt(float(nums_split[2])))
            B_data = ufloat(float(nums_split[3]),sqrt(float(nums_split[3])))
            C_data = ufloat(float(nums_split[4]),sqrt(float(nums_split[4])))
            E_data = ufloat(float(nums_split[5]),sqrt(float(nums_split[5])))
            H_data = ufloat(float(nums_split[6]),sqrt(float(nums_split[7])))
            
            A_sig = ufloat(float(nums_split[7]),float(nums_split[8]))
            B_sig = ufloat(float(nums_split[9]),float(nums_split[10]))
            C_sig = ufloat(float(nums_split[11]),float(nums_split[12]))
            E_sig = ufloat(float(nums_split[13]),float(nums_split[14]))
            H_sig = ufloat(float(nums_split[15]),float(nums_split[16]))

            region_name = nums

            delta_A = get_delta_i(A_data,A_sig)
            delta_B = get_delta_i(B_data,B_sig)
            delta_C = get_delta_i(C_data,C_sig)
            delta_E = get_delta_i(E_data,E_sig)
        
            inverse_delta_H = ufloat(-99,99)
            abs_r = ufloat(-99,99)
            if H_sig !=0:
                inverse_delta_H = H_est_bkg/H_sig

            if inverse_delta_H.n !=-99 and delta_A.n != -99 and delta_B.n != -99 and delta_C.n != -99 and delta_E.n != -99 :
                abs_r = abs(inverse_delta_H * (delta_B + delta_C + delta_E - (2*delta_A)))


            formatted_data_A = print_2dp(A_data)
            formatted_data_B = print_2dp(B_data)
            formatted_data_C = print_2dp(C_data)
            formatted_data_E = print_2dp(E_data)
            formatted_data_H = print_2dp(H_data)

            formatted_sig_A = print_2dp(A_sig)
            formatted_sig_B = print_2dp(B_sig)
            formatted_sig_C = print_2dp(C_sig)
            formatted_sig_E = print_2dp(E_sig)
            formatted_sig_H = print_2dp(H_sig)

            formatted_pred_H = print_2dp(H_est_bkg)
            
            formatted_delta_A = print_2siffigs(delta_A)
            formatted_delta_B = print_2siffigs(delta_B)
            formatted_delta_C = print_2siffigs(delta_C)
            formatted_delta_E = print_2siffigs(delta_E)
            formatted_abs_r = print_2siffigs(abs_r)

        
            f.write('------------------------------------------------------------------------\n')
            f.write('\nSet of regions name: ' + region_name + '\n\n')
            f.write('Number of events in A signal (data): ' + formatted_sig_A + ' ( ' + formatted_data_A + ' )\n')
            f.write('Number of events in B signal (data): ' + formatted_sig_B + ' ( ' + formatted_data_B + ' )\n')
            f.write('Number of events in C signal (data): ' + formatted_sig_C + ' ( ' + formatted_data_C + ' )\n')
            f.write('Number of events in E signal (data): ' + formatted_sig_E + ' ( ' + formatted_data_E + ' )\n')
            f.write('Number of events in H signal (data): ' + formatted_sig_H + ' ( ' + formatted_data_H + ' )\n\n')
            f.write('Expected number of background events in H: ' + formatted_pred_H + '\n\n')
            f.write('delta_A: ' + formatted_delta_A + '\n')
            f.write('delta_B: ' + formatted_delta_B + '\n')
            f.write('delta_C: ' + formatted_delta_C + '\n')
            f.write('delta_E: ' + formatted_delta_E + '\n')
            f.write('|r|: ' + formatted_abs_r + '\n\n')
    f.write('------------------------------------------------------------------------')    
