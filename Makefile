.PHONY: clean

##---

DummyRule:
	@echo "Error - No Rule"

##---

setup:
	@echo "Starting setup"
	@python3 setup.py develop

##---

black:
	@echo "Starting black"
	@black -l 85 setup.py .

##---

cleanSrc:
	@echo "Starting cleanSrc"
	@find . -type f -name "*~" -print -prune | xargs -n 1 rm -rf

cleanVEnv:
	@echo "Starting cleanVEnv"
	@rm -rf VirtualEnvironment
	@rm -rf micro-displaced-signal-contamination.egg-info

cleanPyCache:
	@echo "Starting cleanPyCache"
	@find . -type d -name "__pycache__" -print -prune | xargs -n 1 rm -rf

##---

clean: cleanSrc cleanVEnv cleanPyCache
