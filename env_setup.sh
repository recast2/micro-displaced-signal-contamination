AnalysisDir=$(pwd)
VEnvDir=VirtualEnvironment

SetupVirtualEnvironment () {
    if [ ! -d "./$VEnvDir" ]
    then
	python -m venv $VEnvDir
    else
	echo "Skipping Virtual Environment Creation"
    fi

    . ./$VEnvDir/bin/activate
}

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.22.00-python3-x86_64-centos7-gcc8-opt"
lsetup "python 3.9.11-fix1-x86_64-centos7"

cd $AnalysisDir &&
    SetupVirtualEnvironment
